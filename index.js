
const FIRST_NAME = "Diana Gabriela";
const LAST_NAME = "Feighel";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = new Object();
    cache.pageAccessCounter = function(a = 'home'){
        a = a.toLowerCase();
        if(cache.hasOwnProperty(a)=== true ){
            cache[a] +=1;
            
        } else {
            cache[a] =1;
        }
    };
    cache.getCache = function(){
        return cache;
    };
    
    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

